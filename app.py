import os
import cv2
import logging
from flask import Flask, request, render_template, flash, redirect, url_for
from werkzeug.utils import secure_filename
from PIL import Image
from img2vec_pytorch import Img2Vec
import joblib
import numpy as np

app = Flask(__name__)
app.secret_key = 'your_secret_key'

# Configurations
UPLOAD_FOLDER = 'static/uploads/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
MAX_IMAGE_SIZE = (400, 400)  # Resize images to reduce memory usage

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Ensure the upload folder exists
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

# Load the pre-trained model
try:
    model = joblib.load('svm_flower_classifier.pkl')
except Exception as e:
    logging.error(f"Error loading model: {e}")
    model = None

# Initialize img2vec
try:
    img2vec = Img2Vec(model='resnet-18')
except Exception as e:
    logging.error(f"Error initializing Img2Vec: {e}")
    img2vec = None

def extract_features(image):
    try:
        # Convert to PIL Image and extract features
        image_pil = Image.fromarray(image)
        features = img2vec.get_vec(image_pil)
        return features
    except Exception as e:
        logging.error(f"Error extracting features: {e}")
        return None

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    if 'image' not in request.files:
        flash("No image part in the request")
        logging.error("No image part in the request")
        return render_template('output.html', value=-1, image_path='')

    file = request.files['image']
    if file.filename == '':
        flash("No file selected for uploading")
        logging.error("No file selected for uploading")
        return render_template('output.html', value=-1, image_path='')

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)

        image = cv2.imread(filepath)
        if image is not None:
            image = cv2.resize(image, MAX_IMAGE_SIZE)
            features = extract_features(image)
            if features is not None:
                features = features.reshape(1, -1)
                try:
                    prediction = model.predict(features)
                    # Pass the image path as a query parameter to /output
                    return redirect(url_for('output', value=int(prediction[0]), image_path=filepath))
                except Exception as e:
                    flash("Error making prediction")
                    logging.error(f"Error making prediction: {e}")
                    return render_template('output.html', value=-1, image_path=filepath)
            else:
                flash("Error extracting features from the image")
                logging.error("Error extracting features from the image")
                return render_template('output.html', value=-1, image_path=filepath)
        else:
            flash("Error reading the image")
            logging.error("Error reading the image")
            return render_template('output.html', value=-1, image_path='')
    else:
        flash("File type not allowed")
        logging.error("File type not allowed")
        return render_template('output.html', value=-1, image_path='')

@app.route('/output')
def output():
    value = request.args.get('value', default=-1, type=int)
    # Get the image path from the query parameters
    image_path = request.args.get('image_path')
    return render_template('output.html', value=value, image_path=image_path)

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)
